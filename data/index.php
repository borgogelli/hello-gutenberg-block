<?php

/**
 * Validate the json data file
 */
function validateJSonFile($json_file)
{
    $json_contents = file_get_contents($json_file);
    echo 'CONTENT: ' . PHP_EOL;
    echo $json_contents . PHP_EOL;

    echo 'PHP ARRAY: ' . PHP_EOL;
    $values = json_decode($json_contents, true);
    var_dump($values);

    switch (json_last_error()) {
        case JSON_ERROR_NONE:
            echo ' - No errors' . PHP_EOL;
            break;
        case JSON_ERROR_DEPTH:
            echo ' - Maximum stack depth exceeded' . PHP_EOL;
            break;
        case JSON_ERROR_STATE_MISMATCH:
            echo ' - Underflow or the modes mismatch' . PHP_EOL;
            break;
        case JSON_ERROR_CTRL_CHAR:
            echo ' - Unexpected control character found' . PHP_EOL;
            break;
        case JSON_ERROR_SYNTAX:
            echo ' - Syntax error, malformed JSON' . PHP_EOL;
            break;
        case JSON_ERROR_UTF8:
            echo ' - Malformed UTF-8 characters, possibly incorrectly encoded' .
                PHP_EOL;
            break;
        default:
            echo ' - Unknown error' . PHP_EOL;
            break;
    }
}

// Main
validateJSonFile(__DIR__ . '/wp_employees.json');
