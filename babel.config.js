module.exports = {
presets: [
	["@babel/preset-env",
	{
	  "targets": {
		"node": "current"
	  }
	}],
	"@wordpress/default"
	],
	plugins: [
		'@babel/plugin-proposal-class-properties',
		'@wordpress/babel-plugin-import-jsx-pragma',
		'@babel/transform-react-jsx'
	],
};
