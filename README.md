## Overview

Simple WordPress Gutenberg plugin

![picture](screenshot/screenshot.jpg)

## Usage

Please note that the plugin, after activation in Wordpress, is accessible from the **Layout** section within the Block menu of the Gutenberg Editor

## How to build the zip

Run the pipeline and get it from the [downloads](https://bitbucket.org/borgogelli/hello-gutenberg-block/downloads) section, or alternatively

```
git clone https://borgogelli@bitbucket.org/borgogelli/hello-gutenberg-block.git
npm install
npm run dist
# you will find the the hello-gutenberg-block.zip archive in the dist folder
```

## Download

Get the last version from the [Downloads section](https://bitbucket.org/borgogelli/hello-gutenberg-block/downloads)

## References

 * https://developer.wordpress.org/block-editor
 * https://github.com/moroshko/react-autosuggest#on-suggestion-selected-prop

### Babel

* [config](https://babeljs.io/docs/en/configuration)
