import React from 'react';
import renderer from 'react-test-renderer';

import MyAutosuggest from "../src/MyAutosuggest";


it('MyAutosuggest renders correctly', () => {
	const tree = renderer.create(<MyAutosuggest />).toJSON();
	expect(tree).toMatchSnapshot();
  });
