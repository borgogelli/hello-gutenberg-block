-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Creato il: Ago 25, 2019 alle 14:31
-- Versione del server: 5.7.27-0ubuntu0.18.04.1
-- Versione PHP: 7.2.19-0ubuntu0.18.04.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `wordpress`
--

-- --------------------------------------------------------
 
--
-- Dump dei dati per la tabella `wp_employees`
--

INSERT INTO `wp_employees` (`id`, `first_name`, `last_name`, `position`, `url_image`, `url_github`, `url_linkedin`, `url_xing`, `url_facebook`) VALUES
(1, 'Antonio', 'Cabrini', 'Director', 'https://upload.wikimedia.org/wikipedia/it/thumb/7/79/Antonio_Cabrini_-_Juventus_FC.jpg/318px-Antonio_Cabrini_-_Juventus_FC.jpg', 'https://github.com/borgogelli', 'https://it.linkedin.com/in/andreaborgogelliavveduti', NULL, 'https://www.facebook.com/borgo1'),
(2, 'Karl-Heinz', 'Rummenigge', 'Marketing manager', 'https://upload.wikimedia.org/wikipedia/commons/thumb/7/7e/2015-02-06_Rummenigge_0400.JPG/300px-2015-02-06_Rummenigge_0400.JPG', 'https://github.com/borgogelli', 'https://it.linkedin.com/in/andreaborgogelliavveduti', NULL, 'https://www.facebook.com/borgo1'),
(3, 'Steve', 'Wozniak', 'Developer', 'https://upload.wikimedia.org/wikipedia/commons/thumb/4/4c/SteveWozniak2014_viappy.jpg/330px-SteveWozniak2014_viappy.jpg', 'https://github.com/borgogelli', 'https://it.linkedin.com/in/andreaborgogelliavveduti', NULL, 'https://www.facebook.com/borgo1'),
(4, 'Douglas ', 'Engelbart', 'Chief engineer', 'https://upload.wikimedia.org/wikipedia/commons/thumb/7/78/Douglas_Engelbart_in_2008.jpg/330px-Douglas_Engelbart_in_2008.jpg', 'https://github.com/borgogelli', 'https://it.linkedin.com/in/andreaborgogelliavveduti', NULL, 'https://www.facebook.com/borgo1'),
(5, 'Johann ', 'Radon', 'Clerk', 'https://upload.wikimedia.org/wikipedia/commons/thumb/9/9b/Johann_Radon.png/338px-Johann_Radon.png', 'https://github.com/borgogelli', 'https://it.linkedin.com/in/andreaborgogelliavveduti', NULL, 'https://www.facebook.com/borgo1'),
(6, 'Gerty', 'Radnitz-Cori', 'Quality manager', 'https://upload.wikimedia.org/wikipedia/commons/d/d6/Gerty_Theresa_Cori.jpg', 'https://github.com/borgogelli', 'https://it.linkedin.com/in/andreaborgogelliavveduti', NULL, 'https://www.facebook.com/borgo1');

