<?php

/**
 * Plugin Name: Hello Gutenberg Block
 * Plugin URI: https://bitbucket.org/borgogelli/hello-gutenberg-block
 * Description: This is a plugin demonstrating how to register a new block for the Gutenberg editor.
 * Version: 1.0.2
 * Author: Andrea Borgogelli Avveduti (https://bitbucket.org/borgogelli)
 *
 * @package hello-gutenberg-block
 */

defined('ABSPATH') || exit();

/**
 * Load all translations for our plugin from the MO file.
 */
add_action('init', 'hello_gutenberg_block_esnext_load_textdomain'); // The i18n support
function hello_gutenberg_block_esnext_load_textdomain()
{
    load_plugin_textdomain(
        'hello-gutenberg-block',
        false,
        basename(__DIR__) . '/languages'
    );
}

/**
 * Registers all block assets so that they can be enqueued through Gutenberg in
 * the corresponding context.
 *
 * Passes translations to JavaScript.
 */
function hello_gutenberg_block_esnext_register_block()
{
    if (!function_exists('register_block_type')) {
        // Gutenberg is not active.
        return;
    }

    wp_register_script(
        'hello-gutenberg-block-esnext',
        plugins_url('build/index.js', __FILE__),
        array('wp-blocks', 'wp-i18n', 'wp-element', 'wp-editor'),
        filemtime(plugin_dir_path(__FILE__) . 'build/index.js')
    );

    wp_register_style(
        'hello-gutenberg-block-esnext-editor',
        plugins_url('editor.css', __FILE__),
        array('wp-edit-blocks'),
        filemtime(plugin_dir_path(__FILE__) . 'editor.css')
    );

    wp_register_style(
        'hello-gutenberg-block-esnext',
        plugins_url('style.css', __FILE__),
        array(),
        filemtime(plugin_dir_path(__FILE__) . 'style.css')
    );

    register_block_type('hello-gutenberg-block/employees-esnext', array(
        'style' => 'hello-gutenberg-block-esnext', // style.css
        'editor_style' => 'hello-gutenberg-block-esnext-editor', // editor.css
        'editor_script' => 'hello-gutenberg-block-esnext'
    ));

    if (function_exists('wp_set_script_translations')) {
        /**
         * May be extended to wp_set_script_translations( 'my-handle', 'my-domain',
         * plugin_dir_path( MY_PLUGIN ) . 'languages' ) ). For details see
         * https://make.wordpress.org/core/2018/11/09/new-javascript-i18n-support-in-wordpress/
         */
        wp_set_script_translations(
            'hello-gutenberg-block-esnext',
            'hello-gutenberg-block'
        );
    }
}

add_action('init', 'hello_gutenberg_block_esnext_register_block'); // The Gutenberg block

/**
 * If you’re using non-pretty permalinks, you should pass the REST API route as a query string parameter.
 * For example the route http://oursite.com/wp-json/ would hence be http://oursite.com/?rest_route=/.
 * e.g: http://35.234.116.194/?rest_route=/hr/all&last_name=Re
 */
function my_registered_route()
{
    register_rest_route('hr', 'all', array(
        'methods' => 'GET',
        'callback' => 'custom_hr'
        // 'permission_callback' => function () {
        //  return current_user_can( 'edit_others_posts' );
        //}
    ));
}
function custom_hr($last_name)
{
    global $wpdb;
    $table_name = $wpdb->prefix . 'employees';
    $last_name = mysql_real_escape_string($_GET['last_name']);

    $sql = "SELECT * FROM $table_name";

    if ($last_name) {
        $sql .= " WHERE LOWER(last_name) LIKE LOWER('$last_name%')";
    }
    $sql .= " ORDER BY last_name ASC";

    $result = $wpdb->get_results($sql);
    return rest_ensure_response($result);
}

add_action('rest_api_init', 'my_registered_route'); // The rest api route

function drop_tables()
{
    global $wpdb;
    $table_name = $wpdb->prefix . 'employees';
    $sql = "DROP TABLE IF EXISTS $table_name";
    $wpdb->query($sql);
}

function create_tables()
{
    global $wpdb;
    $table_name = $wpdb->prefix . 'employees';
    $sql = "CREATE TABLE IF NOT EXISTS $table_name (
		id INT NOT NULL AUTO_INCREMENT,
		first_name VARCHAR(50) NULL,
		last_name VARCHAR(50) NULL,
		position VARCHAR(50) NULL,
		description VARCHAR(200) NULL,
		url_image VARCHAR(200) NULL,
		url_github VARCHAR(200) NULL,
		url_linkedin VARCHAR(200) NULL,
		url_xing VARCHAR(200) NULL,
		url_facebook VARCHAR(200) NULL,
		PRIMARY KEY (id));";
    require_once ABSPATH . 'wp-admin/includes/upgrade.php';
    dbDelta($sql); // Useful for creating new tables and updating existing tables to a new structure (see https://developer.wordpress.org/reference/functions/dbdelta/)
}

function pump_data()
{
    global $wpdb;
    $table_name = $wpdb->prefix . 'employees';
    $json_contents = file_get_contents(__DIR__ . '/data/wp_employees.json');
    $values = json_decode($json_contents, true);
    foreach ($values as $value) {
        $wpdb->insert($table_name, $value);
    }
}

function gutenberg_plugin_activate()
{
    create_tables();
    pump_data();
    register_uninstall_hook(__FILE__, 'gutenberg_plugin_uninstall');
}

function gutenberg_plugin_deactivate()
{
    // nothing to do
}

function gutenberg_plugin_uninstall()
{
    drop_tables();
}

register_activation_hook(__FILE__, 'gutenberg_plugin_activate');
register_deactivation_hook(__FILE__, 'gutenberg_plugin_deactivate');
