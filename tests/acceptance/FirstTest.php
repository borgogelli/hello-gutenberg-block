<?php 
/**
 * This is just a stub, for a complete e2e test for this project
 * @ see https://bitbucket.org/borgogelli/hello-e2e-test
 */
class FirstTest
{
    public function _before(AcceptanceTester $I)
    {
    }

    public function frontpageWorks(AcceptanceTester $I)
    {
        $I->amOnPage('/');
        $I->see('Hello World!');  
    }

}
