import Autosuggest from "react-autosuggest";

// When suggestion is clicked, Autosuggest needs to populate the input
// based on the clicked suggestion.
const getSuggestionValue = suggestion => suggestion.last_name;

const renderSuggestion = suggestion => (
	<div>
		{suggestion.last_name} {suggestion.first_name}
	</div>
);

/**
 * An implantation of the Autosuggest reactjs component, that provides a typeahead functionality for the UI
 * The suggestions are fetched via a rest api call
 *
 *@see https://github.com/moroshko/react-autosuggest
 *@see http://react-autosuggest.js.org/
 */
export default class MyAutosuggest extends React.Component {
	constructor() {
		super();

		// Autosuggest is a controlled component.
		// This means that you need to provide an input value
		// and an onChange handler that updates this value (see below).
		// Suggestions also need to be provided to the Autosuggest,
		// and they are initially empty because the Autosuggest is closed.
		this.state = {
			value: "",
			suggestions: []
		};
	}

	onChange = (event, { newValue }) => {
		this.setState({
			value: newValue
		});
	};

	// Autosuggest will call this function every time you need to update suggestions.
	// You already implemented this logic above, so just use it.
	onSuggestionsFetchRequested = async ({ value }) => {
		console.log("onSuggestionsFetchRequested...");
		if (value) {
			let API_URL =
				"http://35.234.116.194/?rest_route=/hr/all&last_name=" + value;
			try {
				let response = await fetch(API_URL, {
					method: "GET",
					headers: {
						"Content-Type": "application/json",
						Accept: "application/json"
					}
				});
				const statusCode = response.status;
				if (statusCode != 200) {
					console.log("HTTP ERROR: " + statusCode + " (GET: " + API_URL);
				} else {
					let json = await response.json();
					console.log("json", json);
					this.setState({ suggestions: json });
				}
			} catch (error) {
				console.log("ERROR", error);
			}
		}
	};

	// Autosuggest will call this function every time you need to clear suggestions.
	onSuggestionsClearRequested = () => {
		this.setState({
			suggestions: []
		});
	};

	onSuggestionSelected = (
		event,
		{ suggestion, suggestionValue, suggestionIndex, sectionIndex, method }
	) => {
		console.log("Selected: " + suggestionValue);
	};

	render() {
		const { value, suggestions } = this.state;

		// Autosuggest will pass through all these props to the input.
		const inputProps = {
			placeholder: "Employee's surname (type 'r')",
			value,
			onChange: this.onChange
		};

		// Finally, render it!
		return (
			<Autosuggest
				suggestions={suggestions}
				onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
				onSuggestionsClearRequested={this.onSuggestionsClearRequested}
				//onSuggestionSelected={this.onSuggestionSelected}
				onSuggestionSelected={this.props.onSuggestionSelected}
				getSuggestionValue={getSuggestionValue}
				renderSuggestion={renderSuggestion}
				inputProps={inputProps}
			/>
		);
	}
}
