/* global wp */
const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const { RichText } = wp.editor;
import MyAutosuggest from "./MyAutosuggest";

// Editable block attributes
const BLOCK_ATTRIBUTES = {
	personName: {
		type: "string",
		default: "Person's Name"
	},
	image: {
		type: "string",
		default:
			"https://dummyimage.com/400x400/c9c7c9/ffffff.jpg&text=Picture+of+the+employee"
	},
	imageData: {
		type: "object",
		default: {}
	},
	employee: {
		type: "object",
		default: {}
	}
};

registerBlockType("hello-gutenberg-block/employees-esnext", {
	title: __("HR: Empoyees", "hello-gutenberg-block"),
	icon: "universal-access-alt",
	category: "layout",
	attributes: BLOCK_ATTRIBUTES,
	edit: props => {
		const {
			attributes: { personName, image, imageData, employee },
			className,
			setAttributes
		} = props;

		const onSuggestionSelected = (
			event,
			{ suggestion, suggestionValue, suggestionIndex, sectionIndex, method }
		) => {
			console.log(
				"DEBUG: " +
					suggestionValue +
					" index " +
					suggestionIndex +
					" " +
					JSON.stringify(suggestion)
			);
			setAttributes({ employee: suggestion });
			let firstName = suggestion.first_name;
			let lastName = suggestion.last_name;
			// let position = suggestion.position;
			// let description = suggestion.description;
			let urlImage = suggestion.url_image;
			let fullName = firstName + " " + lastName;
			setAttributes({ personName: fullName });
			setAttributes({ image: urlImage });
		};

		return (
			<div className={className}>
				{/* <div onClick={hello} className={'hello-gutenberg-block-title'}>Hello from the editor</div> */}
				<MyAutosuggest onSuggestionSelected={onSuggestionSelected} />
				<div>
					<figure class={"hello-gutenberg-block-figure"}>
						<img alt="employee" src={image} {...imageData} />
					</figure>
					<div>
						<span class={"hello-gutenberg-block-label"}>Name:</span>{" "}
						{personName}
					</div>
					<div>
						<span class={"hello-gutenberg-block-label"}>Position:</span>{" "}
						{employee.position}
					</div>
				</div>
			</div>
		);
	},
	save: props => {
		const {
			attributes: { personName, image, imageData, employee },
			className
		} = props;

		return (
			<div className={className}>
				{/* <div className={'hello-gutenberg-block-title'}>Hello from the frontend</div> */}
				<div>
					<figure class={"hello-gutenberg-block-figure"}>
						<img alt="employee" src={image} {...imageData} />
					</figure>
					<div>{personName}</div>
					<div>{employee.position}</div>
				</div>

				{/* Button trigger modal */}
				<button
					type="button"
					class="btn btn-primary"
					data-toggle="modal"
					data-target="#exampleModal"
				>
					Details
				</button>

				{/* Modal */}
				<div
					class="modal fade"
					id="exampleModal"
					tabindex="-1"
					role="dialog"
					aria-labelledby="exampleModalLabel"
					aria-hidden="true"
				>
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="exampleModalLabel">
									Details
								</h5>
								<button
									type="button"
									class="close"
									data-dismiss="modal"
									aria-label="Close"
								>
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body">
								<div class="social-gb">
									<a href={employee.url_github}>Github</a>
								</div>
								<div class="social-ln">
									<a href={employee.url_linkedin}>Linkedin</a>
								</div>
								<div class="social-xg">
									<a href={employee.url_xing}>Xing</a>
								</div>
								<div class="social-fb">
									<a href={employee.url_facebook}>Facebook</a>
								</div>
							</div>
							<div class="modal-footer">
								<button
									type="button"
									class="btn btn-secondary"
									data-dismiss="modal"
								>
									Close
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
});
