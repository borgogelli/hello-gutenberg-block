// https://github.com/archiverjs/node-archiver

// require modules
const fs = require("fs");
const { resolve } = require("path");
const archiver = require("archiver");

console.log("__dirname: " + __dirname); // ISSUE: non è possiible usarlo

const target = resolve(__dirname, "../dist/hello-gutenberg-block.zip");

// create a file to stream archive data to.
var output = fs.createWriteStream(target);
var archive = archiver("zip", {
	zlib: { level: 9 } // Sets the compression level.
});

// listen for all archive data to be written
// 'close' event is fired only when a file descriptor is involved
output.on("close", function() {
	console.log(archive.pointer() + " total bytes");
	console.log(
		"archiver has been finalized and the output file descriptor has closed."
	);
});

// This event is fired when the data source is drained no matter what was the data source.
// It is not part of this library but rather from the NodeJS Stream API.
// @see: https://nodejs.org/api/stream.html#stream_event_end
output.on("end", function() {
	console.log("Data has been drained");
});

// good practice to catch warnings (ie stat failures and other non-blocking errors)
archive.on("warning", function(err) {
	if (err.code === "ENOENT") {
		// log warning
	} else {
		// throw error
		throw err;
	}
});

// good practice to catch this error explicitly
archive.on("error", function(err) {
	throw err;
});

// pipe archive data to the file
archive.pipe(output);

archive.file("index.php", { name: "index.php" });
archive.directory("build/", "build");
archive.directory("src/", "src");
archive.directory("data/", "data");
archive.directory("languages/", "languages");
// archive.glob('./*.css'); // TODO: why it doesn't work ?
archive.file("style.css", { name: "style.css" });
archive.file("editor.css", { name: "editor.css" });

// finalize the archive (ie we are done appending files but streams have to finish yet)
// 'close', 'end' or 'finish' may be fired right after calling this method so register to them beforehand
archive.finalize();
